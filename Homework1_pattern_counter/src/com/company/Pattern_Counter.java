package com.company;

import java.io.*;
import java.util.regex.*;

public class Pattern_Counter {

    public static void main(String[] args) {
        //open file
        RandomAccessFile file = null;
        try {
            file = new RandomAccessFile("test.input", "r");
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        }

        /*create RegEx patterns. Two types of patterns used: "exclusive" and "inclusive"
          Exclusive pattern counts only separate patterns. E.g. for string
          <A8fg3746B   C7rh3d   eud987F> this pattern has only 1 match.
          Inclusive pattern allows treating the end of the previous pattern as
          beginning for the next one. In the example string above, inclusive pattern
          has 2 matches.
         */
        Pattern p_excl = Pattern.compile("([A-z][A-z0-9]+[A-z])\\s{3}\\1");
        Pattern p_incl = Pattern.compile("([A-z][A-z0-9]+[A-z])\\s{3}(?=\\1)");

        int n_excl = 0;
        int n_incl = 0;
        //process files
        try {
            n_excl = CountMatches(file, p_excl);
        } catch (IOException e) {
            System.out.println("Error while reading file!");
        }
        try {
            n_incl = CountMatches(file, p_incl);
        } catch (IOException e) {
            System.out.println("Error while reading file!");
        }
        //print the results
        System.out.println("Number of exclusive matches: " + n_excl);
        System.out.println("Number of inclusive matches: " + n_incl);

    }

    private static int CountMatches(RandomAccessFile file, Pattern p) throws IOException {
        String line;
        int line_matches = 0, file_matches = 0;
        file.seek(0);
        while ((line = file.readLine()) != null) {
            file_matches = 0;
            Matcher m = p.matcher(line);
            while (m.find()) {
                line_matches++;
            }
            file_matches += line_matches;
        }
        return file_matches;
    }
}
