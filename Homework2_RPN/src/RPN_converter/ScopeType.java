package RPN_converter;

/**
 * Created by torni on 10.04.2016.
 */
public enum ScopeType {
    common, if_branch, else_branch
}
