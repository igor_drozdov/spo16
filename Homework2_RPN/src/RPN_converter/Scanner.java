package RPN_converter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Scanner {

    private ArrayList<Token> tokens;
    private Map<String, String> terminals;
    private Map<String, String> keywords;
    private Pattern CommonPattern;
    private String path;
    private BufferedReader reader;

    private String VAR = "[A-Za-z_]+\\w*";
    private String NUM = "0|([1-9][0-9]*)";
    private String OP = "\\+|-|\\/|\\*|\\||&|(<=)|(>=)|(==)|(<>)|~";
    private String ASSIGN_OP = "=";
    private String LPAR = "\\(";
    private String RPAR = "\\)";
    private String LBRACK = "\\{";
    private String RBRACK = "\\}";
    private String IF_KW = "if";
    private String ELSE_KW = "else";
    private String SM = ";";

    public Scanner(String path) throws FileNotFoundException {
        //pre-allocating array for tokens
        tokens = new ArrayList(100);
        //putting terminals name and regexes to HashMaps
        terminals = new HashMap<String, String>();
        keywords = new HashMap<String, String>();
        terminals.put("VAR", VAR);
        terminals.put("NUM", NUM);
        terminals.put("OP", OP);
        terminals.put("ASSIGNOP", ASSIGN_OP);
        terminals.put("LPAR", LPAR);
        terminals.put("RPAR", RPAR);
        terminals.put("LBRACK", LBRACK);
        terminals.put("RBRACK", RBRACK);
        terminals.put("SM", SM);
        keywords.put("IFKW", IF_KW);
        keywords.put("ELSEKW", ELSE_KW);
        //opening file
        this.path = path;
        reader = new BufferedReader(new FileReader(path));
    }

    private void scanFile(String path) throws IOException {
        //creating common pattern string from all token patterns
        StringBuffer CommonPatternBuf = new StringBuffer();
        for (String name : keywords.keySet())
            CommonPatternBuf.append(String.format("|(?<%s>%s)", name, keywords.get(name)));
        for (String name : terminals.keySet())
            CommonPatternBuf.append(String.format("|(?<%s>%s)", name, terminals.get(name)));
        CommonPattern = Pattern.compile(CommonPatternBuf.substring(1));

        //processing file line by line
        String line;
        while ( (line = reader.readLine()) != null)
            scanLine(line);
    }

    private void scanLine(String line) {
        Matcher m = CommonPattern.matcher(line);
        boolean keyword_found;
        String value;
        while (m.find()) {
            keyword_found = false;
            for (String name : keywords.keySet()) {
                if ((value = m.group(name)) != null) {
                    tokens.add(new Token(name, value));
                    keyword_found = true;
                    break;
                }
            }
            if (!keyword_found)
                for (String name : terminals.keySet()) {
                    if ((value = m.group(name)) != null) {
                        tokens.add(new Token(name, value));
                        break;
                    }

                }

        }
    }

    public ArrayList<Token> getTokens() throws IOException {
        scanFile(path);
        return tokens;
    }
}
