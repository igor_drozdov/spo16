package RPN_converter;

import java.util.*;

/**
 * Created by torni on 09.04.2016.
 */
public class RPNConverter {
    private Stack<Token> TokenStack;

    private ArrayList<Token> InputStream;
    private ArrayList<Token> OutputStream;

    private ArrayList<Integer> ENDBLOCK_Tags;
    private ArrayList<Integer> ENDIF_Tags;

    private Map<String, Integer> Precedence;


    public RPNConverter (ArrayList<Token> InputStream) {
        //initialize data structures
        this.InputStream = InputStream;
        OutputStream = new ArrayList<>();
        TokenStack = new Stack<>();
        ENDBLOCK_Tags = new ArrayList<>();
        ENDIF_Tags = new ArrayList<>();
        Precedence = new HashMap<>();
        //setup operator precedence
        Precedence.put("~", 1);
        Precedence.put("*", 2);
        Precedence.put("/", 2);
        Precedence.put("+", 3);
        Precedence.put("-", 3);
        Precedence.put(">=", 4);
        Precedence.put("<=", 4);
        Precedence.put("==", 5);
        Precedence.put("<>", 5);
        Precedence.put("&", 6);
        Precedence.put("|", 7);
        Precedence.put("=", 8);
    }

    //Dijkstra's shunting yard algorithm for 1 token
    private void ShuntingYard(Token CurrentToken) {
            String t_name, t_value;
            switch (CurrentToken.getName()) {
                case "VAR":
                    OutputStream.add(CurrentToken);
                    break;
                case "NUM":
                    OutputStream.add(CurrentToken);
                    break;
                case "OP": case "ASSIGNOP":
                    if (!TokenStack.empty()) {
                        t_name = TokenStack.peek().getName();
                        t_value = TokenStack.peek().getValue();
                        while ((t_name  == "OP") || (t_name == "ASSIGNOP")) {
                            if (Precedence.get(CurrentToken.getValue()) >= Precedence.get(t_value))
                                OutputStream.add(TokenStack.pop());
                            else
                                break;
                            if (!TokenStack.empty()) {
                                t_name = TokenStack.peek().getName();
                                t_value = TokenStack.peek().getValue();
                            }
                            else
                                break;
                        }
                    }
                    TokenStack.add(CurrentToken);
                    break;
                case "LPAR":
                    TokenStack.add(CurrentToken);
                    break;
                case "RPAR":
                    while(TokenStack.peek().getName() != "LPAR")
                        OutputStream.add(TokenStack.pop());
                    TokenStack.pop();
                    break;
                case "SM":
                    while(!TokenStack.empty())
                        OutputStream.add(TokenStack.pop());
                default:
                    break;

            }
    }

    private void ProcessLBRACK(ScopeType Scope) {
        // if we met "{" after in "if" block
        if (Scope == ScopeType.if_branch) {
            OutputStream.add(new Token("OP", "~"));
            //setting up GOTO to end of the block
            OutputStream.add(new Token("INDEX", "ENDBLOCK_INDEX"));
            OutputStream.add(new Token("FGO","!F"));
        }
    }

    private ScopeType ProcessRBRACK(ScopeType Scope, int i) {
        switch (Scope) {
            //if we met "{" in "if" block...
            case if_branch:
                //... and there's "else" keyword after it
                if (InputStream.get(i+1).getName() == "ELSEKW") {
                    //setup GOTO to the end of entire current "if"
                    OutputStream.add(new Token("INDEX", "ENDIF_INDEX"));
                    OutputStream.add(new Token("GO", "!"));
                    //mark next token with ENDBLOCK tag
                    ENDBLOCK_Tags.add(OutputStream.size());
                }
                //...and no "else" keyword after it
                else {
                    //mark next token with ENDBLOCK and ENDIF tags
                    ENDBLOCK_Tags.add(OutputStream.size());
                    ENDIF_Tags.add(OutputStream.size());
                    //because we're out of "if" now, change scope
                    Scope = ScopeType.common;
                }
                break;
            //if we met "{" in "else" block
            case else_branch:
                //mark next token with ENDIF tag
                ENDIF_Tags.add(OutputStream.size());
                //and change scope accordingly
                Scope = ScopeType.common;
                break;
        }
        return Scope;
    }

    private void ConvertToRPN() {
        //set initial scope to common
        ScopeType Scope = ScopeType.common;
        //iterate over all input tokens and process them
        for (int i = 0; i < InputStream.size(); i++) {
            Token CurrentToken = InputStream.get(i);
            switch (CurrentToken.getName()) {
                case "IFKW":
                    Scope = ScopeType.if_branch;
                    break;

                case "ELSEKW":
                    Scope = ScopeType.else_branch;
                    break;

                case "LBRACK":
                    ProcessLBRACK(Scope);
                    break;

                case "RBRACK":
                    Scope = ProcessRBRACK(Scope, i);
                    break;

                default:
                    ShuntingYard(CurrentToken);
            }
        }
    }

    //change tags to specific token indexes
    private void MarkIndexes() {
        int t;
        for (int i = 0; i < OutputStream.size(); i++) {
            //if we encountered unspecified index for next ENDBLOCK tag
            if (OutputStream.get(i).getValue() == "ENDBLOCK_INDEX") {
                //find next closest ENDBLOCK tag index
                for (int j = 0; (t = ENDBLOCK_Tags.get(j)) < i; j++);
                //and specify it
                OutputStream.get(i).setValue(String.format("p%d", t));
            }
            //if we encountered unspecified index for next ENDIF tag
            else if (OutputStream.get(i).getValue() == "ENDIF_INDEX") {
                //find next closest ENDIF tag index
                for (int j = 0; (t = ENDIF_Tags.get(j)) < i; j++);
                //and specify it
                OutputStream.get(i).setValue(String.format("p%d", t));
            }
        }
    }

    public ArrayList<Token> getRPN() {
        ConvertToRPN();
        MarkIndexes();
        return OutputStream;
    }

}
