package RPN_converter;

/**
 * Created by torni on 06.04.2016.
 */

public class Token {

    private String name;
    private String value;

    public Token (String name, String value){
        this.name = name;
        this.value = value;
    }

    public String getName() {return name;}
    public String getValue() {return value;}

    public void setName(String name) {this.name = name;}
    public void setValue(String value) {this.value = value;}

}
