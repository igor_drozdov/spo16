package RPN_converter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        try {
            //scan tokens
            Scanner scan = new Scanner("test_src_code.test");
            ArrayList<Token> tokens = scan.getTokens();
            //print token stream
            for (int i = 0; i < tokens.size(); i++ )
                System.out.println(String.format("%d %s %s", i, tokens.get(i).getName(), tokens.get(i).getValue()));

            System.out.println("\nRPN:");

            //convert token stream to RPN
            RPNConverter conv = new RPNConverter(tokens);
            ArrayList<Token> RPN_tokens = conv.getRPN();

            //print RPN stream
            for (int i = 0; i < RPN_tokens.size(); i++ ) {
                System.out.print(i);
                for (int j = 0; j < RPN_tokens.get(i).getValue().length(); j++)
                    System.out.print(" ");
            }
            System.out.println();
            for (int i = 0; i < RPN_tokens.size(); i++ ) {
                System.out.print(RPN_tokens.get(i).getValue());
                if (i < 10)
                    System.out.print(" ");
                else System.out.print("  ");
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (IOException e) {
            System.out.println("Error while reading file.");
        }
    }
}
